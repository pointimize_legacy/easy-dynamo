# easy-dynamo
Wrap up methods to make DynamoDB easier to use.

## Usages
- Create instance with parameters:
  ```javascript
  const aws = require('aws-sdk');
  const dynamo = require('easy-dynamo')({
    // optional, the AWS SDK instance
    aws,

    // optional, your own logger that includes trace, info, debug, warn, error methods
    logger,

    // optional, the region
    region: 'us-east-1',

    // optional, the Access Key ID & Secret Access Key
    // leave blank to use the credentials stored in the specified AWS instance
    accessKeyId: 'your-access-key-id',
    secretAccessKey: 'your-secret-access-ley'
  });
  ```
- All methods return a `bluebird` Promise.

## Properties

### (Object) dynamoDB
The `AWS.DynamoDB` object.

### (Object) docClient
The `AWS.DynamoDB.DocumentClient` object.

## Methods

### createTables(tableConfigurations, options)
Create tables.

#### Parameters
- (Object[]) `tableConfigurations`: An array of objects passed into `AWS.DynamoDB.createTable()` that you normally do.
- (Object) `options`: (Optional) Options.
- (Boolean) `options.waitForDone`: (Optional) A flag indicates whether to wait for the operation to be done (default `true`).

#### Examples
```javascript
const tableConfigurations = [
  {
    TableName: 'user',
    AttributeDefinitions: [{
      AttributeName: 'id',
      AttributeType: 'S'
    }, {
      AttributeName: 'email',
      AttributeType: 'S'
    }],
    KeySchema: [{
      AttributeName: 'id',
      KeyType: 'HASH'
    }],
    ProvisionedThroughput: {
      ReadCapacityUnits: 1,
      WriteCapacityUnits: 1
    },
    GlobalSecondaryIndexes: [{
      IndexName: 'email-index',
      KeySchema: [{
        AttributeName: 'email',
        KeyType: 'HASH'
      }],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
      }
    }]
  }
];

await dynamo.createTables(tableConfigurations)
// return: [ { tableName: 'user', success: true } ]
```

### deleteTables(tableNames, options)
Delete tables.

#### Parameters
- (String[]) `tableNames`: Array of table names.
- (Object) `options`: (Optional) Options.
- (Boolean) `options.waitForDone`: (Optional) A flag indicates whether to wait for the operation to be done (default `true`).

#### Examples
```javascript
dynamo.deleteTables(['some-table'])
  .then(() => console.log('done'));
```

### waitFor(tableName, options)
Wait for the specified table to be ready.

#### Parameters
- (String) `tableName`: The table name.
- (Object) `options`: (Optional) Options.
- (String) `options.state`: (Optional) The state to wait, could be `tableExists` or `tableNotExists` (default `'tableExists'`).

#### Examples
```javascript
await dynamo.waitFor('some-table');
console.log('The table is ready.');
```

### adjustCapacity(tableName, read, write, options)
Adjust provision throughput of a specific table.

#### Parameters
- (String) `tableName`: The table name.
- (Integer) `read`: The read capacity, should be a integer or null.
- (Integer) `write`: The write capacity, should be a integer or null.
- (Object) `options`: (Optional) Options.
- (Boolean) `options.restorable`: (Optional) A flag indicates whether set the adjustment be restorable by calling `restoreCapacity()` (default `true`).
- (Boolean) `options.waitForDone`: (Optional) A flag indicates whether to wait for the operation to be done (default `true`).

#### Examples
```javascript
await dynamo.adjustCapacity('some-table', 10, null);
console.log('The read provision throughput is adjusted to 10.');
```

### restoreCapacity(tableName, options)
Restore the provision throughput of the specified table.

#### Parameters
- (String) `tableName`: The table name.
- (Object) `options`: (Optional) Options.
- (Boolean) `options.waitForDone`: (Optional) A flag indicates whether to wait for the operation to be done (default `true`).

#### Examples
```javascript
await dynamo.restoreCapacity('some-table');
console.log('The read provision throughput is restored.');
```

### adjustIndexCapacity(tableName, indexName, read, write, options)
Adjust provision throughput of a specific table.

#### Parameters
- (String) `tableName`: The table name.
- (String) `indexName`: The index name.
- (Integer) `read`: The read capacity, should be a integer or null.
- (Integer) `write`: The write capacity, should be a integer or null.
- (Object) `options`: (Optional) Options.
- (Boolean) `options.restorable`: (Optional) A flag indicates whether set the adjustment be restorable by calling `restoreCapacity()` (default `true`).
- (Boolean) `options.waitForDone`: (Optional) A flag indicates whether to wait for the operation to be done (default `true`).

#### Examples
```javascript
await dynamo.adjustIndexCapacity('some-table', 'some-index', 10, null)
console.log('The read provision throughput is adjusted to 10.');
```

### restoreIndexCapacity(tableName, indexName, options)
Restore the provision throughput of the specified table.

#### Parameters
- (String) `tableName`: The table name.
- (String) `indexName`: The index name.
- (Object) `options`: (Optional) Options.
- (Boolean) `options.waitForDone`: (Optional) A flag indicates whether to wait for the operation to be done (default `true`).

#### Examples
```javascript
await dynamo.restoreIndexCapacity('some-table', 'some-index')
console.log('The read provision throughput is restored.');
```

### scanAll(tableName, params, options)
Scan all items that match the conditions (if any).

#### Parameters
- (String) `tableName`: The table name.
- (Object) `params`: The object passed into `documentClient.scan()` that you normally do.
- (Object) `options`: (Optional) Options.
- (Function) `options.callback`: (Optional) The callback function with the signature `function({ items, nextKey, done })`. If `callback` is given, the amount of items will be returned, otherwise, an array of items will be returned.
- (Object) `options.lastEvaluatedKey`: (Optional) The last evaluated key.
- (String[]) `options.selectAttributes`: (Optional) Array of attributes to get.

#### Examples
```javascript
await dynamo.scanAll('users')
// returns [{ id: '0001', name: 'Arthur' }, { id: '0002', name: 'Seoker' }, ...]

await dynamo.scanAll('users', null, {
  callback: ({ items, nextKey, done }) => {
    console.log('Items:', items); // [{ id: '0001', name: 'Arthur' }, { id: '0002', name: 'Seoker' }, ...]
    console.log('nextKey:', nextKey); // { id: '0100' }
    console.log('done:', done); // false
    return Promise.resolve(); // return a Promise, continuing next round of scan
  }
}); // returns 1200 (number of items in the 'users')
```

### scan(tableName, params, options)
Scan items that match the conditions (if any).

#### Parameters
- (String) `tableName`: The table name.
- (Object) `params`: The object passed into `documentClient.scan()` that you normally do.
- (Object) `options`: (Optional) Options.
- (Object) `options.lastEvaluatedKey`: (Optional) The last evaluated key.
- (Number) `options.limit`: The limit number of items of the query operation.
- (String[]) `options.selectAttributes`: (Optional) Array of attributes to get.

#### Examples
```javascript
await dynamo.scan('users', null, { lastEvaluatedKey: { id: '0002' } });
// returns [{ id: '0003', 'name': 'Adu' }, { id: '0004', 'name': 'Keith' }, ...]
```

### queryAll(tableName, key, options)
Query all items that match the specified key.

#### Parameters
- (String) `tableName`: The table name.
- (Object) `key`: The key to query.
- (Object) `options`: (Optional) Options.
- (String) `options.indexName`: (Optional) The index name.
- (Function) `options.callback`: (Optional) See `callback` of `scanAll()`.
- (Object) `options.lastEvaluatedKey`: (Optional) The last evaluated key.
- (String[]) `options.selectAttributes`: (Optional) Array of attributes to get.

#### Examples
```javascript
await dynamo.queryAll('vehicles', { type: 'car' }, { indexName: 'type-index' })
// returns [{ id: '0001', type: 'car', 'name': 'Benz' }, { id: '0002', type: 'car', 'name': 'Volkswagen' }, ...]

await dynamo.queryAll('vehicles', { type: 'car' }, {
  indexName: 'type-index',
  callback: ({ items, nextKey, done }) => {
    console.log('Items:', items); // [{ id: '0001', type: 'car', 'name': 'Benz' }, { id: '0002', type: 'car', 'name': 'Volkswagen' }, ...]
    console.log('nextKey:', nextKey); // null
    console.log('done:', done); // true
    return Promise.resolve(); // return a promise, continuing next round of scan
  }
}); // returns 56 (number of items with type 'car' in the 'vehicles')
```

### query(tableName, key, options)
Query items that match the specified key.

#### Parameters
- (String) `tableName`: The table name.
- (Object) `key`: The key to query.
- (Object) `options`: (Optional) Options.
- (String) `options.indexName`: (Optional) The index name.
- (Object) `options.lastEvaluatedKey`: (Optional) The last evaluated key.
- (Number) `options.limit`: The limit number of items of the query operation.
- (String[]) `options.selectAttributes`: (Optional) Array of attributes to get.

#### Examples
```javascript
await dynamo.query('vehicles', {
  type: 'car'
}, {
  indexName: 'type-index',
  lastEvaluatedKey: { id: '0002', type: 'car' }
});
// returns [{ id: '0003', type: 'car', 'name': 'Ford' }, { id: '0004', type: 'car', 'name': 'Kia' }, ...]
```

### get(tableName, key)
Get specific item that match the specified key.

#### Parameters
- (String) `tableName`: The table name.
- (Object) `key`: The key value pair.

#### Examples
```javascript
await dynamo.get('users', { id: '0001' }); // { id: '0001', name: 'Arthur' }
```

### put(tableName, item)
Put an item into the table.

#### Parameters
- (String) `tableName`: The table name.
- (Object) `item`: The item to put.

#### Examples
```javascript
await dynamo.put('users', { id: '0005', name: 'Weicheng' }); // { id: '0005', name: 'Weicheng' }
```

### update(tableName, key, updatedData, conditions)
(Document missing)

### delete(tableName, key)
(Document missing)

### batchGet(tableName, keys)
(Document missing)

### batchWrite(tableName, putItems, deleteKeys)
(Document missing)

### batchPut(tableName, items)
(Document missing)

### batchDelete(tableName, keys)
(Document missing)

### truncate(tableName)
(Document missing)

## TODO
- [ ] Remove dependencies of `lodash`.
- [ ] Remove dependencies of log, use `debug` instead.
- [ ] Simplify `createTables()`.
- [x] Add `batchGet()`.
- [ ] Write tests.
- [ ] Complete this document.